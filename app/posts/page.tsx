'use client';
import { useEffect, useState } from "react";
import { Post } from "@/type";
import PostLists from "@/app/components/posts/PostLists";
import { fetchPosts } from "@/lib/fetchUsers";

export default function Posts() {
    const [posts, setPosts] = useState<Post[]>([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    useEffect(() => {
        setLoading(true);
        fetchPosts()
            .then((data) => {
                setPosts(data);
                setLoading(false);
            })
            .catch((error) => {
                setError(true);
                setLoading(false);
            });
    }, []);

    if (loading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>Error occurred while fetching user data.</div>;
    }

    if (!posts) {
        return <div>No user found.</div>;
    }

    return (
        <div>
            <h1>Posts</h1>
            <div>
                <PostLists posts={posts} />
            </div>
        </div>
    )
}
