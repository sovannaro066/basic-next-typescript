"use client";
import { useEffect, useState } from "react";
import { User } from "@/type";
import { fetchUserById } from "@/lib/fetchUsers";
import Link from "next/link";


export default function UsersId({ params }: { params: { id: string } }) {
    const [user, setUser] = useState<User | null>(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    useEffect(() => {
        setLoading(true);
        fetchUserById(params.id)
            .then((data) => {
                setUser(data);
                setLoading(false);
            })
            .catch((error) => {
                setError(true);
                setLoading(false);
            });
    }, [params.id]);

    if (loading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>Error occurred while fetching user data.</div>;
    }

    if (!user) {
        return <div>No user found.</div>;
    }

    return (
        <div>
            <h1>My User ID: {params.id}</h1>
            <Link style={{color: 'red'}} href={"/users"}>Back to Users</Link>
            <br />
            <div key={user.id}>
                <h2>Name: {user.name}</h2>
                <p>Email: {user.email}</p>
                <p>Phone: {user.phone}</p>
                <p>Website: {user.website}</p>
            </div>
        </div>
    );
}
