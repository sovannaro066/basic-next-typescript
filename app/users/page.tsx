'use client';
import { User } from "@/type";
import Link from "next/link";
import { useEffect, useState } from "react";
import { fetchUsers } from "@/lib/fetchUsers";

export default function Users() {
    const [users, setUsers] = useState<User[]>([]);

    useEffect(() => {
        fetchUsers().then((data) => {
            setUsers(data);
        });
    }, []);

    return (
        <section>
            <h1>
                <Link href={"/"}>Back to Home</Link>
            </h1>
            <br/>
            {users.map(user => (
                <div key={user.id}>
                    <Link href={`/users/${user.id}`}>
                        {user.name}
                    </Link>
                </div>
            ))}
        </section>
    );
}
