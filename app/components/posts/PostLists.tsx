import { Post } from "@/type";

type Props = {
    posts: Post[];
}

export default function PostLists({ posts }: Props) {
    return (
        <div>
            {posts.map(post => (
                <div key={post.id}>
                    <h2>{post.title}</h2>
                    <p>{post.body}</p>
                </div>
            ))}
        </div>
    )
}
