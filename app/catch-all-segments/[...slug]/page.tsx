type PageProps = {
    params: {
        slug: string[];
    };
};

export default function Page({ params }: PageProps) {
    return (
        <h1>
            My Page: {params.slug[0]} and {params.slug[1]}
        </h1>
    )
}
