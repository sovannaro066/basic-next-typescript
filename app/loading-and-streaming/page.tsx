'use client'
import { Suspense, useEffect, useState } from "react";

type Post = {
    userId: number;
    id: number;
    title: string;
    body: string;
}

export default function LoadingData() {
    const [data, setData] = useState<Post[]>([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://jsonplaceholder.typicode.com/posts');
                const json = await response.json();
                setData(json);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    return (
        <div className="container">
            <h1>Posts</h1>
            <section>
                <Suspense fallback={<p>Loading feed...</p>}>
                    <ul>
                        {data.map((post) => (
                            <li key={post.id}>{post.title}</li>
                        ))}
                    </ul>
                </Suspense>
            </section>
        </div>
    );
}