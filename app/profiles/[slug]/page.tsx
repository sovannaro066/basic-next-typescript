
export default function ProfileId({ params }: { params: { slug: string } }) {
    return (
        <div>My Profile ID: {params.slug}</div>
    );
}