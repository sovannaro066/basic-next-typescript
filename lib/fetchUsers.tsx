
export const fetchUsers = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/users')
    if (!res.ok) {
        throw new Error(res.statusText)
    }
    return await res.json()
}
export const fetchUserById = async (id: string) => {
    const res = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
    if (!res.ok) {
        throw new Error(res.statusText)
    }
    return await res.json()
}

export const fetchPosts = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts')
    if (!res.ok) {
        throw new Error(res.statusText)
    }
    return await res.json()
}
